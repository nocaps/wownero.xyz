# _Mining Wownero_

* * *

## _(Solo-mining only)_

_Ever since [Pool-independence day,](https://forum.wownero.com/t/junkie-jeff-4th-july-hard-fork-information/554) Wownero can only be mined **solo.**_ This means you're going to have to run your own Wownero daemon, which requires the **wownerod** software.

### Setting up your Wownero Daemon

Firstly, download the [latest release of Wownero](https://git.wownero.com/wownero/wownero/releases/latest) for your operating system of choice. After extracting the release, **run `wownerod`.** This daemon will begin synchronizing with the Wownero network. **This will take a long time!**

Once synchronized, you will get a prompt saying:

    You are now synchronized with the network. You may now start wownero-wallet-cli.

Now you can connect to your daemon with a Wownero wallet, like `wownero-wallet-cli` or [Wowlet](https://git.wownero.com/wowlet/wowlet). However in this case, we'll be connecting to it with mining software such as [XMRig](https://xmrig.com).

### Mining with XMRig

While `wownerod` is capable of mining by itself, it's recommended to use dedicated mining software like **XMRig** to mine with the daemon.

Download the [latest release of XMRig](https://github.com/xmrig/xmrig/releases/latest) for your operating system and extract it.

<small>_Pro-tip: You can build XMRig from source and edit the donate.h file to enable **0% dev donation fees.**_</small>  

To mine Wownero, you're going to need two pieces of information:

*   Your **primary public address,**
*   and your **private spendkey.**

This information can either be attained from `wownero-wallet-cli` by running `wallet_info` for the public address, and `viewkey` to see the private spendkey, or from the **Wowlet GUI wallet** by going to **Wallet--> Keys**.

Once you have your public address and private spendkey, you can start mining with XMRig with the following syntax:

    sudo xmrig -a rx/wow -o localhost:34568 -u YOUR_ADDRESS --spend-secret-key SECRET_SPENDKEY --daemon

**Congratulations! You're now mining Wownero!**

## Calculating profitability

By pressing the **h** key while running XMRig, you can see your current hashrate, alongside averages for the last 10s/60s/15m and a max H/s:

    speed 10s/60s/15m 3194.2 3187.3 3104.6 H/s max 3207.0 H/s

Knowing your hashrate and the network difficulty, you can estimate how long it would take for you to find a Wownero block and thus recieve a reward. Simply **take your H/s, divide it by the difficulty, and you'll know the odds of finding a block per second.**

### You can find the difficulty on a block explorer, like [this one.](https://explore.wownero.com)

<img src="static/logo.svg" width="100px">

**[ [Git](https://git.wownero.com/wownero/wownero) | [Wallet](https://git.wownero.com/wowlet/wowlet) | [Mobile Wallet](https://gitlab.com/monerujo-io/wowwallet) | [Swap](https://neroswap.com/) | [Exchange](https://tradeogre.com/exchange/BTC-WOW) ]**

_Take me back [home.](index.html)_