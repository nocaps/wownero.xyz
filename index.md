# _Wownero_

* * *

## _A Cryptocurrency on the Bleeding Edge_

_It's no secret that the modern cryptocurrency space is becoming increasingly corporate, stagnant and non-private._

Most digital decentralized currencies such as [Bitcoin](https://bitcoin.org) and [Ethereum](https://ethereum.org) do nothing to address the increasing privacy concerns that their usage brings: **The ability to track any and all transations on their networks.**

This issue is thankfully resolved through the use of **private cryptocurrencies** based off the [CryptoNote](https://github.com/cryptonotefoundation/cryptonote) protocol, namely [Monero](https://getmonero.org). _However, these projects have **other issues...**_ Namely, these large and established cryptocurrencies suffer from **slow and stagnant development,** and rarely dare to try new and innovative functions.

This is where **Wownero** comes in; It's a fork of Monero, but with a much more active and experimental development.

Wownero is a cryptocurrency that has:

*   **No pool mining** while still having a fair Proof-of-Work system,
*   A **limited supply** rather than Monero's [infinite tail-emission,](https://monero.supply)
*   A **larger ring size** than Monero, making it more private,

**_But most importantly, in their own words..._**

### _It doesn't depend on billionaire shills or lame ass "influencers."_

<img src="static/logo.svg" width="100px">

**[ [Git](https://git.wownero.com/wownero/wownero) | [Wallet](https://git.wownero.com/wowlet/wowlet) | [Mobile Wallet](https://gitlab.com/monerujo-io/wowwallet) | [Swap](https://neroswap.com/) | [Exchange](https://tradeogre.com/exchange/BTC-WOW) ]**

_And read [this page](mining.html) to learn how to mine Wownero._