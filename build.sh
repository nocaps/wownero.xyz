#!/bin/sh
if ! [ -e build/ ]; then
    mkdir build/
fi

if ! [ -e build/static ]; then
    mkdir  build/static
fi

export MKD2HTML="discount-mkd2html"

# Some distros name the binaries differently since
# they have somewhat generic names.
if ! command -v discount-mkd2html 1> /dev/null; then
    if ! command -v mkd2html 1> /dev/null; then
        printf "Could not find a discount binary." 1>&2
        exit 1
    fi

    export MKD2HTML="mkd2html"
fi

for src in *.md; do
    export NEW_FILENAME=${src%.md}.html
    $MKD2HTML -header '<link rel=icon href="static/logo.svg">' -css static/style.css < "$src" > "build/$NEW_FILENAME"
done

cp static/* build/static/